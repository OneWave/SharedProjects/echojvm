package io.onewave.owdesktop

import org.java_websocket.client.WebSocketClient
import org.java_websocket.drafts.Draft_6455
import org.java_websocket.handshake.ServerHandshake
import java.io.Closeable
import java.net.URI

class DeviceScanner(uri: String, clientId: String = "echojvm"): Closeable {
    private val legacyMode = true
    private val connectUri = if (legacyMode) "$uri/scan" else "$uri/scan?client=$clientId"

    private var webSocket: WebSocketClient? = null
    private var connectedCallback: ((Boolean) -> Unit)? = null
    private var scannedDeviceCallback: ((String?) -> Unit)? = null
    private var deviceConnectedCallback: ((Boolean) -> Unit)? = null

    private val actions: Map<String, (String) -> Unit> = mapOf(
        "scan/device/" to {
            if (it.length != "00:00:00:00:00:00".length) {
                println("invalid device address $it")
            } else {
                scannedDeviceCallback?.invoke(it)
            }
        },
        "scan/end" to {
            scannedDeviceCallback.let {
                scannedDeviceCallback = null
                it?.invoke(null)
            }
        },
        "device/connected" to {
            deviceConnectedCallback?.invoke(true)
        },
        "device/disconnected" to {
            deviceConnectedCallback?.invoke(false)
        },
        "heartbeat/ping" to {
            webSocket?.send("heartbeat/pong")
        }
    )

    /*
     * scan protocol
     * ==>
     * scan/start[/period in ms]
     * scan/stop
     * <==
     * scan/device/ab:cd:ef:01:02:03
     * scan/end
     *
     * device
     * ==>
     * device/connect/ab:cd:ef:01:02:03
     * device/disconnect
     * <==
     * device/connected
     * device/disconnected
     */

    private inner class WSClient: WebSocketClient(URI(connectUri), Draft_6455()) {
        override fun onMessage(message: String) {
            actions.keys.find { message.startsWith(it) }?.let {
                actions[it]?.invoke(message.removePrefix(it))
            }
        }

        override fun onOpen(handshake: ServerHandshake) {
            logMessage(LoggerLevel.DEBUG,"Connected to client: ${getURI()}")
            connectedCallback.let {
                connectedCallback = null
                it?.invoke(true)
            }
        }

        override fun onClose(code: Int, reason: String, remote: Boolean) {
            logMessage(LoggerLevel.DEBUG, "Disconnected from: ${getURI()}; reason: $code $reason")
            connectedCallback.let {
                connectedCallback = null
                it?.invoke(false)
            }
        }

        override fun onError(ex: Exception) {
            logMessage(LoggerLevel.DEBUG, "WS Exception: ${ex.stackTraceToString()}")
            connectedCallback.let {
                connectedCallback = null
                it?.invoke(false)
            }
        }
    }

    fun connect(connected: (Boolean) -> Unit) {
        connectedCallback = connected
        webSocket?.closeBlocking()
        webSocket = WSClient().apply { connect() }
    }

    fun scan(period: Int? = null, scannedDevice: (String?) -> Unit) {
        scannedDeviceCallback = scannedDevice
        if (period != null) {
            webSocket?.send("scan/start/${period}")
        } else {
            webSocket?.send("scan/start")
        }
    }

    fun scanStop() {
        scannedDeviceCallback = null
        webSocket?.send("scan/stop")
    }

    fun disconnectRequest() {
        webSocket?.send("device/disconnect")
    }

    fun selectDevice(device: String, connectionState: (Boolean) -> Unit) {
        webSocket?.send("device/connect/$device")
        deviceConnectedCallback = connectionState
    }

    override fun close() {
        scanStop()
        webSocket?.close()
    }
}