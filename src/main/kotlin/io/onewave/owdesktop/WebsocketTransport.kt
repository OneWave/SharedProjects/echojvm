package io.onewave.owdesktop

import io.onewave.api.transport.ProtocolTransport
import io.onewave.utils.hexStringToBytes
import io.onewave.utils.toHexString
import org.java_websocket.client.WebSocketClient
import org.java_websocket.drafts.Draft_6455
import org.java_websocket.handshake.ServerHandshake
import java.io.Closeable
import java.net.URI
import java.nio.ByteBuffer
import java.util.*

class WebsocketTransport(uri: String, clientId: String = "echojvm") : ProtocolTransport, Closeable {
    private val legacyMode = true
    private val connectUri = if (legacyMode) "$uri/protocol" else "$uri/protocol?client=$clientId"

    private var webSocket: WebSocketClient? = null
    private var apiNotificationHandler: ((ByteArray) -> Unit)? = null

    inner class WSClient: WebSocketClient(URI(connectUri), Draft_6455()) {
        override fun onMessage(bytes: ByteBuffer) {
            bytes.array().let {
                logMessage(LoggerLevel.DEBUG, "<< : ${it.toHexString(separator = "")}")
                apiNotificationHandler?.invoke(it)
            }
        }

        override fun onMessage(message: String) {
            if (message == "heartbeat/ping") {
                webSocket?.send("heartbeat/pong")
            } else {
                logMessage(LoggerLevel.DEBUG, "<< : ${message.uppercase(Locale.getDefault())}")
                apiNotificationHandler?.invoke(message.hexStringToBytes())
            }
        }

        override fun onOpen(handshake: ServerHandshake) {
            logMessage(LoggerLevel.DEBUG, "Connected to client: ${getURI()}")
        }

        override fun onClose(code: Int, reason: String, remote: Boolean) {
            logMessage(LoggerLevel.DEBUG, "Disconnected from: ${getURI()}; reason: $code $reason")
        }

        override fun onError(ex: Exception) {
            logMessage(LoggerLevel.DEBUG, "WS Exception: ${ex.stackTraceToString()}")
        }
    }

    init {
    }

    fun connect() {
        webSocket?.closeBlocking()
        webSocket = WSClient().apply { connect() }
    }

    override fun maxPacketLength(): Int {
        return 512
    }

    override fun register(notificationHandler: (ByteArray) -> Unit) {
        apiNotificationHandler = notificationHandler
    }

    override fun send(bytes: ByteArray) {
        logMessage(LoggerLevel.DEBUG, ">> : ${bytes.toHexString(separator = "")}")
        webSocket?.send(bytes)
    }

    override fun close() {
        webSocket?.close()
    }
}