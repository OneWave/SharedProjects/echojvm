package io.onewave.owdesktop

import io.onewave.api.onewave.Onewave

import io.onewave.protocol.DeviceAssociation
import io.onewave.utils.toHexString

import kotlinx.cli.*
import kotlinx.coroutines.runBlocking
import java.io.File
import java.util.*
import java.security.SecureRandom
import kotlin.system.exitProcess

private const val DEFAULT_APPID: Int = 0x11
private const val DEFAULT_KEEPALIVE: Int = 30000

private var appLogLevel: Int = LoggerLevel.INFO.level

private val argParser = ArgParser("echojvm")
private val pin by argParser.option(ArgType.String, shortName = "p", fullName = "pin").required()
private val keyFileName by argParser.option(ArgType.String, shortName = "k", fullName = "key")
private val appId by argParser.option(ArgType.Int, shortName = "a", fullName = "appid").default(DEFAULT_APPID)
private val keepAlive by argParser.option(ArgType.Int, shortName = "s", fullName = "keepalive").default(DEFAULT_KEEPALIVE)
private val debug by argParser.option(ArgType.Boolean, shortName = "d", fullName = "debug").default(false)
private val connectAddr by argParser.option(ArgType.String, shortName = "c", fullName = "connect").default("localhost")
private val runOnce by argParser.option(ArgType.Boolean, shortName = "r", fullName = "runonce").default(false)

fun main(args: Array<String>) = runBlocking {
    argParser.parse(args)

    val pinAssociation = DeviceAssociation()
    pinAssociation.init(pin).await()
    val keyFile = keyFileName?.let { File(it) }
    if (debug) { appLogLevel++ }
    if (appId > UByte.MAX_VALUE.toInt()) {
        logMessage(LoggerLevel.WARN, "Invalid appid, must be between 0 and ${UByte.MAX_VALUE.toInt()}, actual value will be ${appId.toByte()}")
    }

    keyFile ?: logMessage("Missing -key argument, will not use protocol security")

    val connection = OneWaveConnection(pinAssociation, keyFile, keepAlive != 0, keepAlive, appId.toByte(), connectAddr, !runOnce)

    connection.connectToDaemon { success ->
        if (success) {
            connection.startScan { onewave: Onewave ->
                val prng = SecureRandom()
                // Generate up to 254 random bytes
                val data = ByteArray(prng.nextInt(255)){prng.nextInt(255).toByte()}

                runBlocking { onewave.echo(data).await() }
                queryEchoApplet(onewave, data)
            }
        } else {
            exitProgram("Connection to owconnect failed, ensure service is running", 1)
        }
    }
}

fun queryEchoApplet(onewave: Onewave, data: ByteArray) = runBlocking {
    val channel = onewave.getCardTerminal().getCardSession().getBasicChannel(EchoApplet.aid).await()

    val apdu = EchoApplet.echoApdu(data)
    logMessage("${apdu.size} bytes>> ${apdu.toHexString()}")
    val resp = channel.transmit(apdu).await()
    logMessage("${resp.size} bytes<< ${resp.toHexString()}")
}

object EchoApplet {
    val aid: ByteArray = byteArrayOf(0xf2.toByte(), 0x76.toByte(), 0xa2.toByte(), 0x88.toByte(), 0xbc.toByte(),
        0xfb.toByte(), 0xa6.toByte(), 0x9d.toByte(), 0x34.toByte(), 0xf3.toByte(), 0x10.toByte(), 0x01)
    fun echoApdu(data: ByteArray, encryptedData: Boolean = false, extraOperation: ExtraOperation = ExtraOperation.NONE, nbOperations: Int = 0): ByteArray {
        return byteArrayOf(0x00, 0x05, p1(encryptedData, extraOperation), p2(nbOperations)) + data + byteArrayOf(data.size.toByte())
    }

    fun p1(encryptedData: Boolean, extraOperation: ExtraOperation): Byte {
        return ((if (encryptedData) 0x10 else 0x00) or extraOperation.value).toByte()
    }

    private fun p2(nbOperation: Int) = nbOperation.toByte()

    enum class ExtraOperation(val value: Int) {
        NONE(0x00), MEMORY_COPY(0x01), ENCRYPT_DATA(0x02)
    }
}

//fun ByteArray.toHexString() = HexFormat.of().formatHex(this)

fun exitProgram(desc: String? = null, level: Int = 0): Nothing {
    desc?.let { logMessage(LoggerLevel.ERROR, it) }
    exitProcess(level)
}

fun logMessage(str: String) {
    logMessage(LoggerLevel.INFO, str)
}

enum class LoggerLevel(val level: Int) {
    ERROR(0),
    WARN(1),
    INFO(2),
    DEBUG(3),
}

fun logMessage(logLevel: LoggerLevel, str: String) {
    if (appLogLevel >= logLevel.level)
        println(str)
}
