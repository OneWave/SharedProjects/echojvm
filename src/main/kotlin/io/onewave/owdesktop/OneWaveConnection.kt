package io.onewave.owdesktop

import io.onewave.api.onewave.Onewave
import io.onewave.protocol.DeviceAssociation
import io.onewave.protocol.command.CommandPrototype
import io.onewave.utils.hexStringToBytes
import io.onewave.utils.readUShort
import io.onewave.utils.toHexString
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.Closeable
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class OneWaveConnection(
    private val pinAssociation: DeviceAssociation,
    private val keyFile: File?,
    private val keepAlive: Boolean,
    private val keepAliveDuration: Int,
    private val appId: Byte,
    connectAddr: String,
    private val autoReconnect: Boolean = true,
): Closeable {
    private val scanner: DeviceScanner
    private val transport: WebsocketTransport
    private var onewave: Onewave? = null
    private var closedObject = false

    init {
        val server = connectAddr.split(':')
        val websocketAddr =
            if (server.size > 1) {
                "ws://$connectAddr"
            } else {
                "ws://${server[0]}:9191"
            }
        scanner = DeviceScanner(websocketAddr)
        transport = WebsocketTransport(websocketAddr)
    }

    private fun scanEnded(target: String, callback: ((Onewave) -> Unit)?) {
        logMessage("Connecting to $target")
        scanner.selectDevice(target) { connectionState ->
            if (connectionState) {
                startTransport(callback)
            } else {
                logMessage("Disconnected from device")
                onewave?.close()
                if (!autoReconnect) {
                    exitProgram() /* normal program exit */
                }
                if (!closedObject) {
                    startScan(callback)
                }
            }
        }
    }

    private fun searchDevice(addr: String) = runBlocking {
        return@runBlocking pinAssociation.isDevice(addr.getMacAddress()).await()
    }

    fun startScan(callback: ((Onewave) -> Unit)? = null) {
        logMessage("Scanning for device...")
        startScanDevices(callback)
    }

    private fun startScanDevices(callback: ((Onewave) -> Unit)?) {
        scanner.scan(180000) {
            if (it == null) {
                startScanDevices(callback)
            } else {
                logMessage(LoggerLevel.DEBUG, "Scanned: $it")
                if (searchDevice(it)) {
                    scanner.scanStop()
                    scanEnded(it, callback)
                }
            }
        }
    }

    fun connectToDaemon(cb: (success: Boolean) -> Unit) {
        transport.connect()
        scanner.connect(cb)
    }

    override fun close() = runBlocking {
        closedObject = true
        try { onewave?.close() } catch (ignored: Exception) {}
        try { scanner.disconnectRequest() } catch (ignored: Exception) {}
        try { scanner.close() } catch (ignored: Exception) {}
        try { transport.close() } catch (ignored: Exception) {}
    }

    private fun startTransport(callback: ((Onewave) -> Unit)? = null) = runBlocking {
        onewave?.close()

        val builder = Onewave.Builder()
                .transport(transport)
                .fallbackAppId(appId)
                .keepAlive(keepAlive, duration = keepAliveDuration)
                .credentialsCallback { keydata ->
                    keyFile ?: return@credentialsCallback
                    FileOutputStream(keyFile).use {
                        it.write(keydata)
                    }
                }
        var keyData: ByteArray? = null

        onewave = runCatching {
            if (keyFile != null) {
                if (keyFile.exists()) {
                    keyData = withContext(Dispatchers.IO) {
                        FileInputStream(keyFile)
                    }.readBytes()
                } else {
                    logMessage("New pairing...")
                    builder.pair().await()
                    /* Skip verification value */
                }
                builder.secure(keyData).await()
            } else {
                builder.build().await()
            }
        }.getOrElse {  e ->
            exitProgram("Connection initialization failure: ${e.message}", 2)
        }
        logMessage("Connected to target, ready to use")
        callback?.invoke(onewave!!)
    }

    fun protocolScript(script: List<String>, stopOnError: Boolean = true) = runBlocking {
        val responses = mutableListOf<String>()
        for (input in script) {
            if (input.isBlank()) {
                continue
            }
            if (input.dropWhile { it.isWhitespace() }.startsWith("#")) {
                logMessage(input)
                continue
            }
            val bytes = input.hexStringToBytes()
            if (bytes.size < 3) {
                logMessage("Application command must be at least 3 bytes long (cmd id and status bytes)")
                break
            }
            logMessage("CL> ${bytes.toHexString(separator = " ")}")
            val cmd = CommandPrototype(commandId = bytes.readUShort(), status = bytes[2], isSecure = false)
                .buildCommand(payload = bytes.slice(3 until bytes.size).toByteArray())
            val response = onewave?.customCommand(cmd)?.await()
            logMessage("CL< ${response?.bytes?.toHexString(separator = " ")}")
            if (stopOnError && response?.successful != true) {
                logMessage("Aborting script execution: error response or invalid app state")
                break
            }
            responses.add(response?.bytes?.toHexString() ?: "app state error")
        }
        return@runBlocking responses
    }

}

fun String.getMacAddress(): ByteArray {
    val macAddressParts = this.split(":")
    // convert hex string to byte values
    val macAddressBytes = ByteArray(6)
    for (i in 0..5) {
        val hex = macAddressParts[i].toInt(16)
        macAddressBytes[i] = hex.toByte()
    }
    return macAddressBytes
}
